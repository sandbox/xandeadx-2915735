<?php

/**
 * @file
 * Administrative tasks.
 */

/**
 * Page callback: Constructs a form for the Yandex.Disk configuration.
 *
 * @see yandexdisk_menu()
 *
 * @ingroup forms
 */
function yandexdisk_admin($form, &$form_state) {
  $form['yandexdisk_buffer_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Buffer size'),
    '#description' => t('The size of internal buffer for file reading operations (in bytes). Default value is 1048576 bytes which is 1 MB.'),
    '#default_value' => variable_get('yandexdisk_buffer_size', 1048576),
    '#field_suffix' => t('bytes'),
    '#required' => TRUE,
    '#size' => 10,
    '#element_validate' => array('element_validate_integer_positive'),
  );

  return system_settings_form($form);
}
